# Opentype Shaping rules for Malayalam traditional script #
Malayalam script needs advanced text shaping support from the software stack
(shaping engine such as HarfBuzz) and on the font level, known as ‘shaping
rules’ conforming to [OpenType specification](https://learn.microsoft.com/en-us/typography/script-development/malayalam).

## Definitve character set for Malayalam ##
The traditional script for Malayalam has well defined set of conjunct character
set, in addition to the base characters encoded by Unicode. These conjucts form
the complete set of valid conjuncts in Malayalam script.

## OpenType shaping rules ##
All shaping engines (HarfBuzz, Uniscribe, Allsorts etc.) can correctly shape
advanced text layout scripts with fonts implementing shaping rules conforming to
OpenType specification.

The shaping rules developed by Rajeesh for Rachana Institute of Technology
correctly shapes Malayalam traditional scripts, without any known bugs or
incorrect shaping. It supports only the `mlm2` OT script tag. The shaping is
tested to work perfectly with the shaping engines:

- [HarfBuzz](https://harfbuzz.github.io/)
- [Uniscribe/DirectWrite](https://docs.microsoft.com/en-us/windows/win32/intl/uniscribe)
- [Allsorts](https://github.com/yeslogic/allsorts)

and the following applications:

- [XeTeX](https://tug.org/xetex/), LuaHBTeX
- [LibreOffice](https://www.libreoffice.org/)
- [Firefox](https://www.mozilla.org/en-US/firefox/)
- [Chromium](https://www.chromium.org/)/Chrome
- [Qt](https://www.qt.io/) & [Pango](https://gitlab.gnome.org/GNOME/pango) toolkits ([KDE](https://kde.org/), [GNOME](https://www.gnome.org/))
- [Microsoft Word](https://www.microsoft.com/en-in/microsoft-365/word), Wordpad, Notepad, Edge
- [Prince XML](https://www.princexml.com/)
- [Adobe InDesign](https://www.adobe.com/in/products/indesign.html) with HarfBuzz shaper

The shaping rules do not work well with:

- Adobe shaping engine (InDesign CC, 2020)
- Windows XP and applications in that era
- Qt4/Pango < 1.30.1
- ICU shaping engine (Libre/OpenOffice 4.x)

## License ##
The shaping rules program is licensed under OFL 1.1.

## Colophon ##
This shaping rule is an offspring of Rachana movement under the leadership of 
R. Chitrajakumar who founded Rachana Akshara Vedi in 1999. He devised the
‘definitive character set’ of Malayalam based on traditional script.

The naming convention of glyphs (`k1` for `ക`, `k2` for `ഖ` etc.) is devised
by K.H. Hussain in 2005 to form a mnemonic way to represent conjuncts and its
components and to facilitate the coding of glyph substitution of conjuncts.
